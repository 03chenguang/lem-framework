﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.3</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#-L5F.31QU+!!.-6E.$4%*76Q!!(J!!!!3&lt;!!!!)!!!(H!!!!!@!!!!!2J15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;A#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)7`\7)AV=N+LC:FE/+L&lt;BM!!!!-!!!!%!!!!!!2OVK)_?0K4YF7,RW%6;6,V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!0-[*)7V&amp;GE/57N%V$22C,!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#A;(=Z%J_V$9M`+]10PL*8!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!,1!!!$2YH'0A9W"K9,D!!-3-1-T5Q!6E-`VA!.--(U$C$!)=%")%13Q!@@Q.]A!!!!!!!%M!!!%9?*RD9-!%`Y%!3$%S-$"^!^*M;/*A'M;G*M"F,C[\I/,-1-Q#R+Q155;A'.-?))-**!Z6)QK29XI!R#@1T7&amp;$&amp;Q!#!+WT+(-!!!!!$!!"6EF%5Q!!!!!!!Q!!!CQ!!!0Y?*S6EU&amp;L%U%5RW?W1RBE=29**4=&amp;^R"%1IJ"2#KEON'#F6&lt;2&amp;AN'11WN&amp;0311)V66E&lt;&amp;99F%5,_&amp;VV!]S'YX*@87(LR+0!C7ZC!Z#`'^X&gt;EU?J%/\,T@P(HT@W^XXD9.1J:0H4H&gt;"4N'#2EH,LH\].\^1Q47*"\&gt;"!YQZP4Z%``Y*`SQ!C-HDL[)VL*H?IZNS45\9^43XIS&gt;6D-W5]T__8MQ'-CW'8L4+ZWJ39TV/0A\DMVR!:;B(6R&gt;QKB%&lt;Q\5O,N[H&amp;4:B,]9&gt;NQ#D2-HEIN`Z\_B@CT-SZ\FF3SPRN7L]V#U&gt;S7NXFQ%U+6!+F/HD&amp;*(/C^^]=[P=N(S&gt;Q&gt;97X23_ER,2/&gt;FQ!YEV"I+.;&lt;B:#4U'=]X&amp;JD[A.!86J(".-MK)P@F31JGPZ[JC/RW8WTME)JYNCW\&amp;,R"&lt;;8_I$'FX[B2ZSI3=OO=C0=&lt;\G-UG[^X]TPZLZA\E,`IXNFC((UJDF6L`SF;N,&lt;7]?O+5C$&lt;6PHWL&lt;$3F$U/FWA&amp;XZHX0!N\D&lt;=YA]N5VXGGY`#4Y\#G7QKO4ZH*,@0B`=WD1.7W6!%4-ZIE^&amp;+&gt;9D9KIZC/T6*M6G0T#-V[V&amp;&amp;N8A[(`@:R\^MH\".DJ!@T]'"]DF"#ITV+HOK^37"$_Z,?2&lt;;UQ"(A-?X&lt;(.E`40=ZV*Q&gt;U3]9_\E2=;?HYZ$\GL'OF0;&gt;A]#G:A@YDO&lt;,Q&amp;U;]T6A3`NP!B]T9CZLPD$LF%:?@@AP`Q%Q)&gt;P4!!!!$"9!A"!!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!%!!!"$%W,D!!!!!!$"9!A!!!!!1R.CYQ!!!!!!Q7!)!1!!!%-49O-!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!,#Q!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!,L6_*L1M!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!,L6]V.45VC;U,!!!!!!!!!!!!!!!!!!!!!0``!!!,L6]V.45V.45V.9GN#Q!!!!!!!!!!!!!!!!!!``]!C6]V.45V.45V.45V.47*L1!!!!!!!!!!!!!!!!$``Q"@8T5V.45V.45V.45V.@[*!!!!!!!!!!!!!!!!!0``!&amp;_*C6]V.45V.45V.@\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9F@.45V.@\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*8[X_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q#*C9G*C9G*C@\_`P\_`IG*!!!!!!!!!!!!!!!!!0``!!"@8YG*C9G*`P\_`IGN8Q!!!!!!!!!!!!!!!!!!``]!!!!!8YG*C9H_`IG*8Q!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!&amp;_*C9G*.1!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"@.1!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!B#*5%Z($1I;#A!!!!V*3%23!!!!$Q!!!"!)"A!!!-F7*11!!!!"=V*(1A#OTBTJ!!!!"'&gt;"45%!!,'0#`RB"1!!!!FQ3&amp;FT!!!/QQ!!$M-"RW_I:!!!!"FU26BU5W^G&gt;(&gt;B=G5!5'&amp;J&lt;H1O4E65)(9T,D5O.5G+`/!!!!'!352"6$B0F:0,SU&amp;B%-&lt;HL`5`O)3E7&amp;CA+,*BRU)M:#=,M2'F&amp;*(\.=K&gt;"@6]0;/D\Y&lt;PGXJ[TZG:X\RHXHG0N.NNV/NV6+P60[N7K[(6;E%;D19KF1J#I2"M.BP-:P/PMFAM'A]%!CC8SS#H=$!96)V')\SSV7K&amp;3#3#=$B]BZP.*KR7[VP1+-I.\(9\S!F\&gt;DA=,X@](H1[H3!HX7Y8,J@L8\$&lt;\19Z[@@\](A]#F_PV\&gt;CHN@L"4FB$T[@4_(T_;QSG5Q`:-39RXRS-JV/Y@@\&amp;&gt;ZON[L.:P-&amp;.PR=;2Q8/6EM&amp;DJD'E&gt;B;,F=;I(00D\4/#JSMF[P%9V'V4G&lt;T&lt;4C-R'AR7)RE"._9DQ?6_&gt;E-M&amp;A-(AK&amp;K=F%AFN4@&lt;\0:,**/&lt;TO4L'Y\'?*%@2[82U\@6['![(D`.)J6)A*[@4#96#!@F]8PNY:3R?,"9VHZTM&gt;DPNN61K):@,):P.)J0*)*V/0]2XCH(_2'TP=$D=&lt;^DR?-4N&gt;POT,J?,NP%"B/"-'W5Z8$I!!!!!356/2+Z#9))!!!)AC6"/2QU+'AI!!!!.35B%5A!!!!]!!!!1#!9!!!$*6C5%!!!!!8.32U)!LMY=[1!!!!2H15V"!!#RDQP]915!!!!*=%B:=Q!!$M-!!!\$!=&gt;PK'1!!!!:&gt;%69&gt;&amp;.P:H2X98*F!&amp;"B;7ZU,EZ&amp;6#"W-SYV,D6*CPTA!!!"E%F%161Y4Z74S[O"524&amp;TV`D@`1)34%Q1&amp;%??&lt;_6RQ!$B9'9C&amp;++5C*EI/36C6KXN@8&gt;\MNV\W"VPP::P\XX/@N];D;&lt;941;94!9`&amp;H$Y2$4[22K0"[DX_`$Z`0":$*"L^@`+)0")0M?DQ?^8A`E"0:[P;*%)I&amp;WO`V5R7)2A5!!@L``!5]G%RC.RJ?AFJ1&amp;T'9TS#G?W7+R`&amp;LR;T&gt;7KR8EV(Q_B]VG_R&gt;MN^N"4CU7#TA=$I'&lt;T?:,U?&gt;U/E&amp;/L69LO&amp;QOA?PVOECHUXW4NE=@`?45:L/"W_U7O&amp;+JC-LF]C&gt;9CX/FD_-CJX;\H=S919Z#5T[@FQ1@9`SGD[-CJ`&lt;\09,"I!1TG1T3[@24Z8)Z]98$9:"4B]-"]8B=AKF5#L&amp;9\+G9H$\/GJQ[H5ZAC^FM6M[;4#92D59F?SA5ED53C5A"\4Y+B1,)K?PVCF;LB5;D!;WN:U_5S4O&gt;DPD*K?0RC/VWCW[XCVKNBGKV+B6+J&gt;+\N.PG0H_C^8K.]`H]?''8SQ8X_`X0ONVO7#[8?!.+*_Q-LA*=0Q!!!!"*25Z%LE*AAA!!!.%7!)!!!!!!!1!%!!!!!1!!!!!!"!!!!!ND&lt;'&amp;T=V.U=GFO:R9!A!!!!!!"!!A!-0````]!!1!!!!!!%D]K/DII35Z46&amp;*]5U^$3U65+1!!!!!!!!!.:'FT='RB?5:J&lt;(2F=B9!A!!!!!!"!!5!"Q!!!1!!!!%A!!!!!!!!!!!,;8.36%.P&lt;H2F?(17!)!!!!!!!1!&amp;!!=!!!%!!!!!!!!!!!!!!!!!#82Z='6$&lt;'&amp;T=R9!A!!!!!!"!!A!-0````]!!1!!!!!!"5FO=X2S!!!!!!!!!!!!!'Y7!)!!!!!!!1!%!!!!!1!!!!!!!A!!!!VE;8.Q&lt;'&amp;Z2GFM&gt;'6S&amp;A#!!!!!!!%!"1!(!!!"!!!!!"!)!!!!!!!!!!RT&gt;7*5?8"F1WRB=X-7!)!!!!!!!1!)!$$`````!!%!!!!!!!2598.L!!!!!!!!!!!!&lt;B9!A!!!!!!"!!1!!!!"!!!!!!!#!!!!$72J=X"M98F';7RU:8)7!)!!!!!!!1!&amp;!!=!!!%!!!!!%!A!!!!!!!!!$(.V9F2Z='6$&lt;'&amp;T=R9!A!!!!!!"!!A!-0````]!!1!!!!!!"&amp;2B=WM!!!!!!!!!!!&gt;[!!!6U(C=X6B&gt;&lt;"26&amp;$ZXO\O&gt;X2;:\1^NB&lt;JX[WR&amp;J&gt;!1"52%7Q;RCO7HN&amp;55=&gt;P:UM;S3`9(%#.&amp;8:M5R*CA*"IR0BCC4TS11$4%9&amp;02:+LCC];@J-!,U3&gt;D1C4)&gt;$XXTMT/T'\`5KMRNMH*:0:]ZZR\TH@/0&lt;M!F;.CN7M=$GJ!R'PYM&amp;%$P[)3A*%'!9S`_E%1W]G@1#JKC!90#?XC4[ZRMEC$5E7N&amp;RLF)@A.N&lt;/8MC@)6J)3,[/K6[R"9XY.ZCNK6?"2;5S54CW3BDSGV16QOXC5D,O?E)*`#%=T=81)G=6-"BL)/"!ZZ(:H[D:(9N'-R.\['I1;&lt;N+HA3CL]R,3W"VI%6U0=Z.EG/QAXZMG!5UOBN/H4VOAA![KZW'M1AT:A;A%':Y#5S;LN5FJ&lt;!H(_$E'`32-0X*Q`"C,H9(SI?7S+C)5=@O.)T/MC&gt;-?YLAL6[YA$K7"[^7A1BJL&amp;GK%S]K0HHF&lt;-G?"!"H:+71`SLZ"8C4$A6:7";Z&lt;BI5)LS:C'T[X;8"82H5^#W\46TV]SMPA.MOQ"MN!(O:F]-F$)FBV_(C;/MDOE&lt;L;&gt;@XJ:#K;I0%?WNU@33&lt;JHE4@XEAK3J6)+F*9I1&gt;EV&lt;?3H:YZY_3!7]&amp;.[O!4?\&lt;D=0,E35Q!3AO[&amp;K&amp;6UFA/N]!YD:FRR=IY]WJF\E(-H(R^WS[7P@"KF]F:$_&gt;M#Z@^8"\E=ID,*IP,SZ(,!4%`8!?8\ZV\,K^!CBX-YT+=)1+I5`"SJ1[S=:FV1#W=G1+T#D%$$CY,[+@7^$-6F_]LZ,*A_MJR_@DRYQY=6G2VDMNO1H1O:SZG&lt;W:P-E90:\]HL8#6-^L$%?V9$FGFH*JHU=HP[)/FP]B-`Q:&amp;$5T-Y4.4ZZ_[1?@RAI[7NC;;C#&lt;D[52XF-9CO[.U5TK66Z9X.8A%2["1)9]&amp;NZC%+!-8X!`P]9D]8+V%A]?1L-OFM5U9E$4E9GI#:+!;DG%KMNFM))D+,L&amp;'D]X&amp;T/O025+.,WBY?UO$D9J+G:U37\])5!'XQ9#:Z7S!:RG.'K&lt;ZS4)B/)4=ENA&lt;8Y.UXG`9,.8A=:RX;0*O06?FOME3#%):GBE9'%!T++U)C_B)5%^H&amp;22H+&amp;=*-3H7'@R)#X8[#@*67&lt;J$,!3H;O[%3+'&gt;MOL"A5$U;-[T;)L"DQ=M.SNXD:^P&gt;(15!U.J1:_661'B_[2,,FN['$I)B[:&amp;2X*I9E.\Q9@IFZTI!Q=/)"KFB?YSU5YIT9@GSG*"O`0G'0O\%^\.&gt;9W,!R6&amp;=53-8;.AVY3@B0"WQ'FWV8C08.MFKT895@0V&amp;!L-:CFSMBI%N00$DT_DH@4A_\SA&lt;JVH=P#0,UX/Y@-8NO=,NO@0^6+RK6F50VCU`BF;&amp;3&lt;OM/1/&gt;\I^8XG_NE,LN9@WCQZRY[!V.$&lt;B6'6B(=;DHI7NTAPI#,O!.P$GL81W\^:JGN&gt;L.'`*_FCEKT`;FM*\*[^DE?_P9&lt;\R]KSU"LA,\I:ST-VND[&amp;"*KX=7!Q/1!!:T&amp;2#4/99X&amp;^)&gt;FV6S4*6F./L`HWLO2&lt;#!R\6\^2/HL`Z0(`6C'_!SNT5BJY*TVC*+J3LB*B+TE7U-"K(;H&lt;GKG`08D6X2C49[T;#&lt;;X1#?:"AAU_Q`*E['X'+VR2P&gt;$-5N#"ZA^$%_&gt;;M=GV1R-O/S8)N&lt;5TZ&amp;LROH1C%9XFXQS8.(B*5&gt;VM6PM&gt;O]V];)1(]G&lt;VD2MXM#9I&lt;&lt;0;B&lt;-;X`A;-KHQ\4NX7&gt;6^';O\1BL42VQDD\A309?A&amp;#W&gt;/X=/,;'=;&amp;S8IALF+C%G=]HN,KS$LMJO[B#44F5\VV\2\\5C7T4),LBH^N&amp;UT4S;,E=U/0ESDMFXXMJY:=(E]ZB4\8KD.?'O,\-^,\5^.^CGY,@/#_P6AD7U")`@.0U;/GCH=)F/97`^I/M#&amp;&lt;+`[6_H,I3%!@/R,P=FST#"7V!`=]COV1\YU,E"R5VCS[@9:G)SGW"Y84.E&gt;KE_2?8YNEDSO=,&amp;:Q^&lt;@%4\YF/&amp;'CO-1)L.EC3MR=&gt;=7TVQ$"&lt;#"`I3;.^]-&amp;N(L%30(X&lt;O0ME*&gt;B]0X)+GDMV[^UE6\DZ?.&amp;K,B..T-OH7QV=:JD)H7U_[=/NRY`^#G/&gt;=(S&lt;9O@&gt;/M$Y%M1L4]G_@D8`NZ694(3B=(YIRFH,QT^X[5-T8"S_O$^[QZ!NX?JTLQQPWU-I92.S/HXSGQ?Z!.@NC/V,T\:LGN3EE*OW,L6H7P*9G^U3\_XL[IEG;[IV3`EEK4P@V^H8XMI?)IP!0^P9F5OF)0_XOD=2CU8[GX:?E(3WU/R(&amp;&lt;3'ZF,&lt;UU/@D;;L%;3S?-MQ_4S0=Z",;WN)A.WX:P&gt;`5.T[A0@%%BU6C#P/6H*%T&amp;FCK.Z,C*J&lt;[KI8NY&gt;7#N36BA\?Q8UN_R&lt;,2`!&lt;@0(G$+T.M](H'V&gt;5U39&gt;PG6G(N]V:BW`\"TK]`4`3Y2WT\`$/W8&lt;Y%Z.U_&amp;0`1I@\]DL=[_TQJS@N].&lt;`?Y@\?9@\7GU`Q1A`85B&lt;VXO&lt;*'?H5B$89[9'.+C5VZ6^)\:ANEK&amp;&amp;G'^?-)V4IA'#Y14QL72\X+`MIZMT#%T&amp;]HCIALJH;"1`2&gt;X6S!K!!!!!!!%!!!!9!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!5S!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!$Z&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!$&gt;!!!!"A!?!$@`````!!E7!)!!!!!!!1!%!!!!!1!!!!!!!!!Y1(!!&amp;12598.L!!!"!!!&amp;4EF%16%7!)!!!!!!!1!%!!!!!1!!!!!!!!V$&gt;8*S:7ZU17^598.L!$B!=!!6"&amp;2B=WM!!!%!!!6/352"529!A!!!!!!"!!1!!!!"!!!!!!!!$%6O97*M:52P6'&amp;T;Q!!$5!+!!&gt;$&gt;8*S:7ZU!""!)1N&amp;&lt;G&amp;C&lt;'64&gt;'&amp;U:1!K1&amp;!!"!!"!!)!!Q!%'F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T!!!"!!5!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!/29!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!"1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$=*N=[!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E7!)!!!!!!!1!&amp;!!=!!!%!!.QGVTI!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!"629!A!!!!!!"!!A!-0````]!!1!!!!!"/1!!!!A!(A!X`````Q!*&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!/%"Q!"5%6'&amp;T;Q!!!1!!"5Z*2%&amp;2&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!.1X6S=G6O&gt;%&amp;P6'&amp;T;Q!Y1(!!&amp;12598.L!!!"!!!&amp;4EF%16%7!)!!!!!!!1!%!!!!!1!!!!!!!!R&amp;&lt;G&amp;C&lt;'6%&lt;V2B=WM!!!V!#A!(1X6S=G6O&gt;!!11#%,27ZB9GRF5X2B&gt;'5!(A!X`````Q!%&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!0%"Q!!Y&amp;37ZT&gt;()!!1!&amp;&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!76EF413"S:8.P&gt;8*D:3"O97VF)%^V&gt;!!!,%"1!!5!!1!#!!-!"!!''F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T!!!"!!=!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E7!)!!!!!!!1!&amp;!!-!!!%!!!!!!"5!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!"6B9!A!!!!!!)!"Y!.`````]!#29!A!!!!!!"!!1!!!!"!!!!!!!!!$B!=!!6"&amp;2B=WM!!!%!!!6/352"529!A!!!!!!"!!1!!!!"!!!!!!!!$5.V=H*F&lt;H2"&lt;V2B=WM!/%"Q!"5%6'&amp;T;Q!!!1!!"5Z*2%&amp;2&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!-27ZB9GRF2'^598.L!!!.1!I!"U.V=H*F&lt;H1!%%!B#U6O97*M:6.U982F!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!#R!5!!&amp;!!%!!A!$!!1!"BJ15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O&lt;(:D&lt;'&amp;T=Q!!!1!(!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!+!"!!!!!%!!!!DA!!!#A!!!!#!!!%!!!!!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"E1!!!RVYH*63SU\#5"!^U*9XC!J&amp;5+1ERI5,%D%_9FQ512-W5M7YR&gt;I79[S5N,@%*8`JRI`1,^$J1VX92,S4X0&lt;/H(00G7E".,'*QQ^;3)O9AV9-P,]([UC?IMR@K][DHR-O_LX/:21SXX6NWZCQDO7$&amp;_&lt;FTC&lt;KH7HU!BLS=A&lt;*]#I5Z79W+!_:SIRPKXSEV2/3,!D^C=.MSAF2'0'G0_R)NO&amp;9LKU:UE2^-K3"SQ!4\[`&lt;,]=%C&gt;=5:4DS&lt;BG&gt;$J42Q7CXP&gt;]S:ZKJ/IY95&gt;+9C&lt;;M1##./$B33^3\JOMQQZ;MM?4TJ+H^-+-'*&amp;VF+C%&amp;)!/&gt;8,UB21=O0'+0HGEZ#7ZMXC-FTTHLCA5*QRL4."3#ZJ"(A80V-8;QY)B*)A.+.,'&amp;0U?%EN^-E7):+VDFW,./&gt;)7;7U+*XD4#=)2O`;PJA"@T6)F"7Q)_GQ:Q#]`=/5F)K+*''3`7C@)6K4"_:XYK'XY%=YSB(ALS98.6**&amp;'&amp;G7)K'#.`K1'62K"&gt;&gt;+N5(C??/^D?!92_Q11`(90!!!!!!!!:1!"!!)!!Q!%!!!!3!!5"!!!!!!5!2%""Q!!!&amp;%!&amp;!1!!!!!&amp;!%2!1=!!!";!"1%!!!!!"1"%1%(!!!!9Y!!B!#!!!!5!2%""QB4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!(J!!!!3&lt;!!!!)!!!(H!!!!!!!!!!!!!!!#!!!!!U!!!%B!!!!"V-35*/!!!!!!!!!7R-6F.3!!!!!!!!!9"36&amp;.(!!!!!!!!!:2$1V.5!!!!!!!!!;B-38:J!!!!!!!!!&lt;R$4UZ1!!!!!!!!!&gt;"544AQ!!!!!!!!!?2%2E24!!!!!!!!!@B-372T!!!!!!!!!AR735.%!!!!!!!!!C"W:8*T!!!!"!!!!D241V.3!!!!!!!!!JB(1V"3!!!!!!!!!KR*1U^/!!!!!!!!!M"J9WQY!!!!!!!!!N2-37:Q!!!!!!!!!OB.4E&gt;*!!!!!1!!!PR46&amp;)A!!!!!A!!!S2'5%BC!!!!!!!!!W"'5&amp;.&amp;!!!!!!!!!X275%21!!!!!!!!!YB-37*E!!!!!!!!!ZR#2%BC!!!!!!!!!\"#2&amp;.&amp;!!!!!!!!!]273624!!!!!!!!!^B%6%B1!!!!!!!!!_R.65F%!!!!!!!!"!")36.5!!!!!!!!""271V21!!!!!!!!"#B'6%&amp;#!!!!!!!!"$Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!0````]!!!!!!!!!S!!!!!!!!!!!`````Q!!!!!!!!$=!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!"%!!!!!!!!!!!`````Q!!!!!!!!%9!!!!!!!!!!$`````!!!!!!!!!5Q!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!!`````Q!!!!!!!!'M!!!!!!!!!!4`````!!!!!!!!!^Q!!!!!!!!!"`````]!!!!!!!!$\!!!!!!!!!!)`````Q!!!!!!!!0]!!!!!!!!!!H`````!!!!!!!!"!Q!!!!!!!!!#P````]!!!!!!!!%(!!!!!!!!!!!`````Q!!!!!!!!1M!!!!!!!!!!$`````!!!!!!!!"%1!!!!!!!!!!0````]!!!!!!!!%7!!!!!!!!!!!`````Q!!!!!!!!4=!!!!!!!!!!$`````!!!!!!!!#/!!!!!!!!!!!P````]!!!!!!!!)]!!!!!!!!!!$`````Q!!!!!!!!M%!!!!!!!!!!4`````!!!!!!!!$3A!!!!!!!!!"@````]!!!!!!!!/!!!!!!!!!!!'`````Q!!!!!!!!ZU!!!!!!!!!!$`````!!!!!!!!$OA!!!!!!!!!!0````]!!!!!!!!7;!!!!!!!!!!!`````Q!!!!!!!":Q!!!!!!!!!!$`````!!!!!!!!&amp;HA!!!!!!!!!!0````]!!!!!!!!7C!!!!!!!!!!!`````Q!!!!!!!"&lt;Q!!!!!!!!!!$`````!!!!!!!!&amp;PA!!!!!!!!!!0````]!!!!!!!!=-!!!!!!!!!!!`````Q!!!!!!!"QY!!!!!!!!!!$`````!!!!!!!!(%!!!!!!!!!!!0````]!!!!!!!!=&lt;!!!!!!!!!#!`````Q!!!!!!!"Y%!!!!!":15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2J15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!%!!%!!!!!!!!!!!!!!1!C1&amp;!!!"J15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!@``!!!!!1!!!!!!!1!!!!!"!#*!5!!!'F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T!!!"!!!!!!!"`````A!!!!!"&amp;F"15V^*&lt;H.U=H6N:7ZU,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!9!(A!X`````Q!*&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!/%"Q!"5%6'&amp;T;Q!!!1!!"5Z*2%&amp;2&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!.1X6S=G6O&gt;%&amp;P6'&amp;T;Q!Y1(!!&amp;12598.L!!!"!!!&amp;4EF%16%7!)!!!!!!!1!%!!!!!1!!!!!!!!R&amp;&lt;G&amp;C&lt;'6%&lt;V2B=WM!!!V!#A!(1X6S=G6O&gt;!!11#%,27ZB9GRF5X2B&gt;'5!;A$RX#;[*!!!!!);5&amp;"48UFO=X2@1E^18T:@-4)V,GRW9WRB=X-75&amp;"48UFO=X2@1E^18T:@-4)V,G.U&lt;!!Q1&amp;!!"!!"!!)!!Q!%(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"1!!!!4`````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2:15&amp;.@37ZT&gt;(*V&lt;76O&gt;#ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!!!!1!!!!!!!Q!!!!!)!"Y!.`````]!#29!A!!!!!!"!!1!!!!"!!!!!!!!!$B!=!!6"&amp;2B=WM!!!%!!!6/352"529!A!!!!!!"!!1!!!!"!!!!!!!!$5.V=H*F&lt;H2"&lt;V2B=WM!/%"Q!"5%6'&amp;T;Q!!!1!!"5Z*2%&amp;2&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!-27ZB9GRF2'^598.L!!!.1!I!"U.V=H*F&lt;H1!%%!B#U6O97*M:6.U982F!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!'Q!]&gt;QGVTI!!!!#'F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T&amp;F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZD&gt;'Q!-E"1!!5!!1!#!!-!"!!'(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"Q!!!!5!!!!!!!!!!1!!!!)!!!!$`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"&amp;F"15V^*&lt;H.U=H6N:7ZU,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!")!!!!!2:15&amp;.@37ZT&gt;(*V&lt;76O&gt;#ZM&gt;G.M98.T!&amp;"53$!!!!!E!!%!"!!!"E.P&lt;7VP&lt;B:15&amp;.@37ZT&gt;(*V&lt;76O&gt;#ZM&gt;G.M98.T!!!!!!</Property>
	<Item Name="PPS_Inst_BOP_6_125.ctl" Type="Class Private Data" URL="PPS_Inst_BOP_6_125.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Private" Type="Folder">
		<Item Name="List clear.vi" Type="VI" URL="../Private/List clear.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!![1(!!$A6*&lt;H.U=A!"!!57!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="NI 6341 Voltage output with slope_bop.vi" Type="VI" URL="../Private/NI 6341 Voltage output with slope_bop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#/!!!!"Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!"6!#A!06G^M&gt;'&amp;H:3".98AA-4"7!"Y!]!!$!!-!"!!&amp;!Q!!%!!!$1%!!!I!!!!+!!!!!!%!"A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="NI 6341 Voltage output_bop.vi" Type="VI" URL="../Private/NI 6341 Voltage output_bop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#/!!!!"Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!"6!#A!06G^M&gt;'&amp;H:3".98AA-4"7!"Y!]!!$!!-!"!!&amp;!Q!!%!!!$1%!!!I!!!!+!!!!!!%!"A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Query function mode.vi" Type="VI" URL="../Private/Query function mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!N!"A!%47^E:1!!(A!X`````Q!%&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!0%"Q!!Y&amp;37ZT&gt;()!!1!'&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!76EF413"S:8.P&gt;8*D:3"O97VF)%^V&gt;!!!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!!^!"A!)2H6O9X2J&lt;WY!!$J!=!!/"5FO=X2S!!%!"B9!A!!!!!!"!!1!!!!"!!!!!!!!&amp;6:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"*&lt;A"5!0!!$!!$!!1!"1!(!!1!"!!%!!1!#!!%!!E!#A-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!#A!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Query function.vi" Type="VI" URL="../Private/Query function.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!^!"A!)2H6O9X2J&lt;WY!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"B9!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!![1(!!$A6*&lt;H.U=A!"!!97!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!5!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Query list points location.vi" Type="VI" URL="../Private/Query list points location.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!V!#A!'5'^J&lt;H2T!!!?!$@`````!!17!)!!!!!!!1!%!!!!!1!!!!!!!!!]1(!!$A6*&lt;H.U=A!"!!97!)!!!!!!!1!%!!!!!1!!!!!!!":736.")(*F=W^V=G.F)'ZB&lt;75A4X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/E"Q!!Y&amp;37ZT&gt;()!!1!'&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!66EF413"S:8.P&gt;8*D:3"O97VF)%FO!&amp;1!]!!-!!-!"!!&amp;!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Query ouput on or off.vi" Type="VI" URL="../Private/Query ouput on or off.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!R!)1&gt;04C^02E9`!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"B9!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!![1(!!$A6*&lt;H.U=A!"!!97!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!5!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Query Voltage limit POS and NEG.vi" Type="VI" URL="../Private/Query Voltage limit POS and NEG.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="RMS to p-p value.vi" Type="VI" URL="../Private/RMS to p-p value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$2!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!^!#A!*5#V1)&amp;:B&lt;(6F!":!5!!$!!!!!1!##%6S=G^S)%FO!!!01!I!#6*.5S"W97RV:1"5!0!!$!!$!!1!"1!%!!1!"!!%!!1!"A!%!!=!"!)!!(A!!!U)!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!+!!!!!!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Send command to BOP.vi" Type="VI" URL="../Private/Send command to BOP.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;N!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"2!-0````]+5X2S;7ZH)%^V&gt;!!!(A!X`````Q!%&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!0%"Q!!Y&amp;37ZT&gt;()!!1!'&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!76EF413"S:8.P&gt;8*D:3"O97VF)%^V&gt;!!!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!"*!-0````]*5X2S;7ZH)%FO!$J!=!!/"5FO=X2S!!%!"B9!A!!!!!!"!!1!!!!"!!!!!!!!&amp;6:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"*&lt;A"5!0!!$!!$!!1!"1!(!!1!"!!%!!1!#!!%!!E!#A-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!%+!!!!#A!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set AC amplitude and frequency list to BOP.vi" Type="VI" URL="../Private/Set AC amplitude and frequency list to BOP.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(B!!!!%A!?!$@`````!!17!)!!!!!!!1!%!!!!!1!!!!!!!!![1(!!$A6*&lt;H.U=A!"!!!7!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!$U!+!!F346-A&gt;G&amp;M&gt;75!%5!+!!N4&gt;'&amp;S&gt;#""&lt;G&gt;M:1!21!I!#V"S&lt;X2F9X1A5'^T!$R!=!!/"5FO=X2S!!%!!"9!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!!1!!!!41!9!$6&gt;B&gt;G6G&lt;X*N)(2Z='5!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!#1!+!!M)28*S&lt;X)A37Y!!!^!#A!*2H*F=86F&lt;G.Z!!^!#A!*27ZE)%&amp;O:WRF!"&amp;!#A!,5(*P&gt;'6D&gt;#"/:7=!&amp;E"1!!-!#1!+!!M*28*S&lt;X)A4X6U!'Q!]!!1!!%!!A!$!!1!"1!'!!=!"Q!(!!A!"Q!-!!U!$A!0!"!$!!%)!!!+!!!!#A!!!!A!!!!)!!!!$1!!!!I!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!I!!!!)!!!!#!!!!!A!!!!.#Q!!!!%!%1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set BOP to external.vi" Type="VI" URL="../Private/Set BOP to external.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!![1(!!$A6*&lt;H.U=A!"!!57!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set function mode.vi" Type="VI" URL="../Private/Set function mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!,1!9!"%VP:'5!!$J!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;6:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"*&lt;A"5!0!!$!!$!!1!"!!'!!1!"!!(!!1!#!!*!!1!#A-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!I!!!!!!!!!#A!!!!I!!!!!!!!!#A!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Set function voltage or current.vi" Type="VI" URL="../Private/Set function voltage or current.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!":!5!!$!!!!!1!##%6S=G^S)%FO!!![1(!!$A6*&lt;H.U=A!"!!57!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!1!"A!%!!1!"Q!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!+!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Set List Count.vi" Type="VI" URL="../Private/Set List Count.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!41!I!$6*F='6B&gt;#"/&gt;7VC:8)!/E"Q!!Y&amp;37ZT&gt;()!!1!&amp;&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!66EF413"S:8.P&gt;8*D:3"O97VF)%FO!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!+!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set output on or off.vi" Type="VI" URL="../Private/Set output on or off.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!-1#%(4UYP4U:')!![1(!!$A6*&lt;H.U=A!"!!57!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!%!!=!"!!)!!E$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!!I!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set protect POS and NEG.vi" Type="VI" URL="../Private/Set protect POS and NEG.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!21!I!#V"S&lt;X2F9X1A4G6H!"&amp;!#A!,5(*P&gt;'6D&gt;#"1&lt;X-!/E"Q!!Y&amp;37ZT&gt;()!!1!&amp;&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!66EF413"S:8.P&gt;8*D:3"O97VF)%FO!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"Q!)!!E!#A!,!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!A!!!!+!!!!#!!!!!A!!!!+!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set Voltage or current limit POS and NEG.vi" Type="VI" URL="../Private/Set Voltage or current limit POS and NEG.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;[!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!21!I!#V"S&lt;X2F9X1A4G6H!".!#A!.5(*P&gt;'6D&gt;#"1&lt;X-A)!![1(!!$A6*&lt;H.U=A!"!!57!)!!!!!!!1!%!!!!!1!!!!!!!"6736.")(*F=W^V=G.F)'ZB&lt;75A37Y!6!$Q!!Q!!Q!%!!1!"A!%!!1!"!!(!!A!#1!+!!M$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!#!!!!!I!!!!)!!!!#!!!!!I!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Set waveforms Amplitude and Frequency.vi" Type="VI" URL="../Private/Set waveforms Amplitude and Frequency.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!'9!]1!!!!!!!!!#'F"S;7VB=HF1&lt;X&gt;F=F.V=("M?3ZM&gt;G.M98.T&amp;V"15V^5?8"F8UFQ6W&amp;W:62Z='5O9X2M!#N!&amp;A!$"&amp;.J&lt;G5)6(*J97ZH&lt;'5'5X&amp;V98*F!!!)6W&amp;W:62Z='5!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!41!I!$6!N5#""&lt;8"M;82V:'5!$U!+!!F'=G6R&gt;76O9XE!/E"Q!!Y&amp;37ZT&gt;()!!1!&amp;&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!66EF413"S:8.P&gt;8*D:3"O97VF)%FO!&amp;1!]!!-!!-!"!!%!!9!"!!%!!=!#!!*!!I!#Q!-!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!#!!!!!A!!!!+!!!!#!!!!!A!!!!+!!!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Set waveforms start and end angle.vi" Type="VI" URL="../Private/Set waveforms start and end angle.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$R!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"0&gt;81!!!^!"A!)2H6O9X2J&lt;WY!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!01!I!#56O:#""&lt;G&gt;M:1!21!I!#V.U98*U)%&amp;O:WRF!$J!=!!/"5FO=X2S!!%!"29!A!!!!!!"!!1!!!!"!!!!!!!!&amp;6:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"*&lt;A"5!0!!$!!$!!1!"!!'!!1!"!!(!!1!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!A!!!!!!!!!#A!!!!A!!!!)!!!!#A!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="PPS_Inst_Close.vi" Type="VI" URL="../PPS_Inst_Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$Z!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!"Q;5&amp;"48UFO=X2@1E^18T:@-4)V,GRW9WRB=X-!!"615&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5A;7Y!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!9$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!(!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="PPS_Inst_Open.vi" Type="VI" URL="../PPS_Inst_Open.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%X!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T!!!75&amp;"48UFO=X2@1E^18T:@-4)V)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!("J15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O&lt;(:D&lt;'&amp;T=Q!!&amp;6"15V^*&lt;H.U8U*05&amp;]W8T%S.3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
	</Item>
	<Item Name="PPS_Inst_SetCurrent.vi" Type="VI" URL="../PPS_Inst_SetCurrent.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T!!!75&amp;"48UFO=X2@1E^18T:@-4)V)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!I!"U.V=H*F&lt;H1!0%"Q!"Y!!"Q;5&amp;"48UFO=X2@1E^18T:@-4)V,GRW9WRB=X-!!"615&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*)!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="PPS_Inst_SetOutputState.vi" Type="VI" URL="../PPS_Inst_SetOutputState.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"15V^*&lt;H.U8U*05&amp;]W8T%S.3ZM&gt;G.M98.T!!!75&amp;"48UFO=X2@1E^18T:@-4)V)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31#%.4X6U=(6U27ZB9GRF0Q!]1(!!(A!!("J15&amp;.@37ZT&gt;&amp;^#4V"@.F]R-D5O&lt;(:D&lt;'&amp;T=Q!!&amp;6"15V^*&lt;H.U8U*05&amp;]W8T%S.3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
</LVClass>
